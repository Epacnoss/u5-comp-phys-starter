#!/bin/env python3

from pylab import *

def func (x):
    # y = tan(x**2 + 9)
    y = sqrt(x**2 + x**3)
    return y

n = int(input("Enter number: "))
print("Silly of n = ", func(n))

a = linspace(0, 10, 11)
print(func(a))
plot(a, func(a), 'go')
plot(a, func(a) + 1, 'rx')
show()